let arr =[25,36,49,81]

let arrSqr=arr.map((curElem)=>{
    return Math.sqrt(curElem)
})
console.log(arrSqr);


let arr1=[2,3,4,6,8]

let arr3=arr1.map((crntElm)=>{
    return crntElm*2

}).filter((crntElm)=>{
    return crntElm>10
})

console.log(arr3);

//in one line
let arr4=arr1.map((curElm)=>curElm*2).filter((curElm)=>curElm>10).reduce((acuumulator,curElem,index,arr)=>acuumulator+=curElem);
console.log(arr4);