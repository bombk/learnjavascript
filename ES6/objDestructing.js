const myBioData={
    myFname:'bom',
    myLname:'thapa',
    myAge:26
}
// let age=myBioData.myAge;
// console.log(age);
let {myFname,myLname,myAge}=myBioData;

console.log(myBioData.myAge);


let myName="vinod";
const myBio={
    [myName]:"hello how are you", //[myName] become variable for myName="vinod"
    36:"is my age"
}
console.log(myBio);

//no need to write key and value for same name

let myNam='ram'
let myAg=25

const mydata={myNam,myAg}
console.log(mydata);

//spread operator
//for same value array
//we can call same value from other array

const color=['red','green','blue','white']
const myColor=['red','green','blue','white','gray','orange']

const myFavColor=[...color,'yellow','black']
console.log(myFavColor);