console.log(Math.PI);

console.log(Math.round(2342.234));

console.log(Math.pow(2,3)); //2*2*2

console.log(Math.sqrt(25)); //op 5

//absolute (positive)
console.log(Math.abs(-55)); //55
console.log(Math.abs(-23.23));
console.log(Math.abs(4-6));

console.log(Math.ceil(4.1)); //5
console.log(Math.floor(4.9)); //4

console.log(Math.min(1,3,2,42,123,123));
console.log(Math.max(1,3,2,42,123,123));

console.log(Math.random()*10);


console.log(Math.trunc(4.5)); //op 4
console.log(Math.trunc(-4.5)); //op -4

