var myName="bom bk"
console.log(myName);
console.log(typeof(myName)) //give strings

//number
var myAge=26
console.log(typeof(myAge));
var myBalance=23456432345324544354532
console.log(typeof(myBalance));

//boolean
var iAmThapa =true;
console.log(typeof(iAmThapa));

console.log(10+"20"); //op=1020
console.log(9-"5");  //op=4 -> it is bug


console.log("java"+"script"+"node"); //op=javascriptnode

console.log(" "+0);  //op=9

console.log("java" - "script"); //op=NaN (not a number)

//1 is true ,0 is false

console.log(true+true); //op =2

//null
var iamUseless=null;
console.log(iamUseless);   //null
console.log(typeof(iamUseless)); //op =object
//undefined
var varName;
console.log(varName);   //undefined
console.log(typeof(varName));  ///undefined
 
//NaN
//it is property of global object
myNumber="mynum"

console.log(isNaN(myNumber));

if (isNaN(myNumber)){
    console.log("please enter valid number");
}
