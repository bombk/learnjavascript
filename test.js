
var InternetIPoEDefaultParam = {
    "1.Enable": 1,
    "1.AddressingType": "DHCP",
    "1.ConnectionTrigger": "AlwaysOn",
    "1.ConnectionType": "IP_Routed",
    "1.NATEnabled": 1,
    "1.X_HW_IPv4Enable": 1,
    "1.X_HW_IPv6Enable": 1,
    "1.X_HW_IPv6.IPv6Prefix.1.Origin": "PrefixDelegation",
    "1.X_HW_IPv6.IPv6Address.1.Origin": "DHCPv6",
    "1.X_HW_SERVICELIST": "INTERNET",
    "1.X_HW_VLAN": 21,
    "1.Name": "GACS_INTERNET_IPoE",
    "1.MaxMTUSize":1500


}
for(let elem in InternetIPoEDefaultParam){
    console.log(elem);
    console.log(InternetIPoEDefaultParam[elem]);
}
