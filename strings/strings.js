let myName ="bom bk"
let myChangename='bom bk'

let ytName =new String("Technical Education")

console.log(myName);
console.log(ytName);

console.log(myName.length); //op 6

//scape characters
let any="we are so-called \"viking\" from the north."
let any1='we are so-called \"viking\" from the north.'
let any2='we are so-called \'viking\'from the north.'
let any3="we are so-called 'viking'from the north."

console.log(any);
console.log(any1);
console.log(any2);
console.log(any3);

//find string
console.log(any.indexOf('are')); // if found give index in string otherwise give -1
//3
console.log(any3.lastIndexOf('the'));

//search string in string

console.log(any.search("are")); //give index

//extracting string parts
//slice,substring,substr

var str="apple,banana,kiwi"

let res=str.slice(0,4)
console.log(res); //op appl

//write 10 charater only

let myTweet="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin inter"
let myact=myTweet.slice(0,4)
console.log(myact);

//subsrting
let mysbstring=myTweet.substring(0,4)
console.log(mysbstring);
//Substr()

let mySubstr=myTweet.substr(0,4)

let newstr="hello world world"
let repstr=newstr.replace("world","bom") //only replace first string
console.log(repstr);

let repall=newstr.replace(/world/g,"bom") //replace all
console.log(repall);


//extrating string parts
//charAt(position)
//charCodeAt(position)
//Property access []

console.log(newstr.charAt(4));  //give o
console.log(newstr.charCodeAt(4)); //give unicode o=111

console.log(newstr.charCodeAt(newstr.charAt(newstr.length-1))); //give unicode o=111

//property access
var str1=" hello World "
console.log(str1[9]); //op =l

console.log(str1.toUpperCase());
console.log(str1.toLowerCase());
console.log(str1.concat(" ",str));
console.log(str1.trim());

///convert string to array
console.log(str1.split("|"));